.. role:: raw-html-m2r(raw)
   :format: html


Emulator: Testing your app
--------------------------

:raw-html-m2r:`<font color=#636363>The</font>` **Emulator** :raw-html-m2r:`<font color=#636363>lets you test mixed reality applications on your PC without a real Nreal Light glasses and computing unit. With Emulator, you can speed up app development by testing, iterating and debug without having to build and deploy the app to Nreal device. You could simply</font>` **use Unity to test your app by importing an Emulator Prefab.**

:raw-html-m2r:`<font color=#636363>Controlling the Emulator is very similar to common 3D video games. You can use keyboard and mouse to control the head pose movement, controller rotation, trackable planes or images in 3D space.</font>`

**In this section, you will learn:**


* 
  :raw-html-m2r:`<font color=#636363>How to enable Unity to debug MR apps as an Emulator.</font>`

* 
  :raw-html-m2r:`<font color=#636363>How to simulate input events that are usually read by Nreal Light glasses and controller sensors by using your keyboard and mouse when debugging.</font>`

:raw-html-m2r:`<br />`

Prerequisites
^^^^^^^^^^^^^


* Understanding NRSDK concept and working flow
* :ref:`Import NRSDK into Unity<introducing_NRSDK>`
* Have the code and resources of your app ready

:raw-html-m2r:`<br />`

Find Emulator in NRSDK package
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

NRSDK > Emulator

.. image:: ../../../images/Emulator/em1-1.jpg

:raw-html-m2r:`<br />`

Emulator Structure
^^^^^^^^^^^^^^^^^^

**Emulator** :raw-html-m2r:`<font color=#636363>folder contains all the code and resources of Emulator, here is a brief introduction for you to lookup. </font>`


* **Editor** :raw-html-m2r:`<font color=#636363>contains the script used for modifying Unity Editor.</font>`  
* **Image** :raw-html-m2r:`<font color=#636363>contains the UI image resources to show controller state.</font>`
* **Material** :raw-html-m2r:`<font color=#636363>contains the materials for TrackableImage and TrackablePlane in Emulator.</font>`
* **Model** :raw-html-m2r:`<font color=#636363>contains the room model used in the sample scene.</font>`
* **Prefabs** 

  * ``NRTrackableImageTarget.prefab`` :raw-html-m2r:`<font color=#636363>used for simluating the detection of images.</font>`
  * ``NRTrackablePlaneTarget.prefab``  :raw-html-m2r:`<font color=#636363>used for simulating the detection of planes. </font>`

* **Resources** :raw-html-m2r:`<font color=#636363>used for dynamic loading resource. </font>`
* **Scene** :raw-html-m2r:`<font color=#636363>contains two demos for testing TrackableImage and TrackablePlane.</font>`
* **Script** 

  * ``NativeEmulator.cs``  :raw-html-m2r:`<font color=#636363>used for calling low-level API.</font>`
  * ``NREmulatorManager.cs`` :raw-html-m2r:`<font color=#636363>is the script  for managing the life cycle of Emulator. </font>`
  * ``NREmulatorController.cs`` :raw-html-m2r:`<font color=#636363>simulates the input of controller.</font>`
  * ``NREmulatorHeadPose.cs`` :raw-html-m2r:`<font color=#636363>simulates the headpose movement.</font>`
  * ``NRTrackableImageBehaviour.cs`` :raw-html-m2r:`<font color=#636363>is the script to simulate the trackable images.</font>`
  * ``NRTrackablePlaneBehaviour.cs`` :raw-html-m2r:`<font color=#636363>is the script to simulate the trackable planes. </font>`
  * ``NRTrackableObserver.cs`` :raw-html-m2r:`<font color=#636363>is the observer of trackable target.</font>`
  * ``TrackableFoundTest.cs``  :raw-html-m2r:`<font color=#636363>is a testing script that. </font>`

:raw-html-m2r:`<br />`

Simulating the 6DoF Head Pose
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* 
  :raw-html-m2r:`<font color=#636363>Use WSAD on the keyboard to simulate the movement of the position of the head.</font>`

    :raw-html-m2r:`<font color=#636363>W -> move forward.</font>`
    :raw-html-m2r:`<font color=#636363>S -> move backward.</font>`
    :raw-html-m2r:`<font color=#636363>A -> move left.</font>`
    :raw-html-m2r:`<font color=#636363>D -> move right.</font>`

* 
  :raw-html-m2r:`<font color=#636363>Press Space key and Use the Mouse to simulate the rotation of the head.</font>`

* 
  :raw-html-m2r:`<font color=#636363>Sample Code:</font>`

  .. code-block:: c#


           void UpdateHeadPosByInput()
           {
               float mouse_x = Input.GetAxis("Mouse X") * HeadRotateSpeed;
               float mouse_y = Input.GetAxis("Mouse Y") * HeadRotateSpeed;
               Vector3 mouseMove = new Vector3(m_CameraTarget.transform.eulerAngles.x - mouse_y, m_CameraTarget.transform.eulerAngles.y + mouse_x, 0);
               Quaternion q = Quaternion.Euler(mouseMove);
            m_CameraTarget.transform.rotation = q;

               Vector3 p = GetBaseInput();
               p = p * HeadMoveSpeed * Time.deltaTime;
               Vector3 pos = p + m_CameraTarget.transform.position;
            m_CameraTarget.transform.position = pos;

               // Call api to simulate the headpose movement
               NREmulatorManager.Instance.NativeEmulatorApi.SetHeadTrackingPose(pos, q);
           }

:raw-html-m2r:`<br />`

Simulating Controller Input
^^^^^^^^^^^^^^^^^^^^^^^^^^^

:raw-html-m2r:`<font color=#636363>There is also a controller UI showing the emulator controller state, including touch movement and button event, at the lower-right corner of the Game window.</font>`

  
.. image:: ../../../images/Emulator/em1-2.jpg



* 
  :raw-html-m2r:`<font color=#636363>Simulating Controller Rotation</font>`
    :raw-html-m2r:`<font color=#636363>Press Shift + Move Mouse</font>`
* 
  :raw-html-m2r:`<font color=#636363>Simulating Controller Button </font>`
    :raw-html-m2r:`<font color=#636363>Mouse left button -> Select Button</font>`
    
    .. image:: ../../../images/Emulator/em1-3.jpg

    :raw-html-m2r:`<font color=#636363>Mouse right button -> Home Button</font>`
     
    .. image:: ../../../images/Emulator/em1-4.jpg

    :raw-html-m2r:`<font color=#636363>Mouse middle button -> App Button</font>`
    
    .. image:: ../../../images/Emulator/em1-5.jpg



* :raw-html-m2r:`<font color=#636363>Simulating Controller Touch (Swipe Gesture)</font>`
    :raw-html-m2r:`<font color=#636363>Keyboard Left/ Right/ Up/ Down Arrow -> Swipe Left/ Right/ Up/ Down</font>`

    .. image:: ../../../images/Emulator/em1-6.jpg


    .. image:: ../../../images/Emulator/em1-7.jpg


    .. image:: ../../../images/Emulator/em1-8.jpg


    .. image:: ../../../images/Emulator/em1-9.jpg


* 
  :raw-html-m2r:`<font color=#636363>Sample Code:</font>`  ``NREmulatorController.cs``

  .. code-block:: c#

           // Control the rotation of controller
           void UpdateControllerRotateByInput()
           {
               float mouse_x = Input.GetAxis("Mouse X") * HeadRotateSpeed;
               float mouse_y = Input.GetAxis("Mouse Y") * HeadRotateSpeed;

               Vector3 mouseMove = new Vector3(m_Target.transform.eulerAngles.x - mouse_y, m_Target.transform.eulerAngles.y + mouse_x, 0);
               Quaternion q = Quaternion.Euler(mouseMove);
               m_Target.transform.rotation = q;
               NREmulatorManager.Instance.NativeEmulatorApi.SetControllerRotation(new Quaternion(q.x, q.y, q.z, q.w));

           }

:raw-html-m2r:`<br />`

Tutorial: Simulating Headpose, Controller & Trackable Object
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Run the Sample
~~~~~~~~~~~~~~

:raw-html-m2r:`<font color=#636363>First , </font>`\ ``Assets/NRSDk/Emulator/Scenes/TrackableImageEmulator.unity`` :raw-html-m2r:`<font color=#636363>and</font>` ``Assets/NRSDk/Emulator/Scenes/TrackablePlaneEmulator.unity`` :raw-html-m2r:`<font color=#636363>are two samples for the Emulator. Developers could directly play the scene in Unity Editor or build them into apk and run on an Nreal Device.</font>`

Create your own
~~~~~~~~~~~~~~~


1. :raw-html-m2r:`<font color=#636363>Make sure you have</font>` ``Assets/NRSDK/Prefabs/NRCamreaRig.prefab`` :raw-html-m2r:`<font color=#636363>and</font>` ``NRSDK/Prefabs/NRInput.prefab`` :raw-html-m2r:`<font color=#636363>in the scene.</font>` 


  * :raw-html-m2r:`<font color=#636363>When Unity Editor under runtime, the</font>` ``NREmulatorHeadPose.prefab`` :raw-html-m2r:`<font color=#636363>will be automatically loaded by</font>` ``NRCameraRig.prefab`` :raw-html-m2r:`<font color=#636363>into the scene for simulating the head pose data.</font>`
  * :raw-html-m2r:`<font color=#636363>When Unity Editor under runtime, the</font>`\ ``EmualatorController.Prefab`` :raw-html-m2r:`<font color=#636363>will be automatically loaded by</font>` ``NRInput.prefab`` :raw-html-m2r:`<font color=#636363>into the scene for simulating the controller input data. </font>`

:raw-html-m2r:`<br />`

2. :raw-html-m2r:`<font color=#636363>Place</font>` ``Assets/NRSDK/Emulator/Prefabs/NRTrackableImageTarget.prefab`` :raw-html-m2r:`<font color=#636363>or</font>` ``Assets/NRSDK/Emulator/Prefabs/NRTrackablePlaneTarget.prefab`` :raw-html-m2r:`<font color=#636363>to simulate the trackalbe images and planes.</font>`


  * ``TrackableObserver.cs`` :raw-html-m2r:`<font color=#636363>is attached to every</font>`\ ``NRTrackableImageTarget.prefab`` :raw-html-m2r:`<font color=#636363>and</font>` ``NRTrackablePlaneTarget.prefab``\ :raw-html-m2r:`<font color=#636363>. You need to register your own logic of Trackable found and lost into TrackableObserver.</font>`


  .. image:: ../../../images/Emulator/em1-10.jpg


:raw-html-m2r:`<br />`


3. :raw-html-m2r:`<font color=#636363>In</font>` ``/Assets/NRSDK/Emulator/Scripts/TrackableFoundTest.cs`` :raw-html-m2r:`<font color=#636363>, you could find the sample for the register event.</font>`


  * :raw-html-m2r:`<font color=#636363>Sample Code:</font>` ``TrackableFoundTest.cs``

  .. code-block:: c#

     public class TrackableFoundTest : MonoBehaviour {

       // Observer of the registed event
       public TrackableObserver Observer;
       // Showing GameObject on the detected Trackable object
       public GameObject Obj;

       void Start () 
       {
           Obj.SetActive(false);
           Observer.FoundEvent += Found;
           Observer.LostEent += Lost;
       }

       private void Found(Vector3 pos, Quaternion qua)
       {
           Obj.transform.position = pos;
           Obj.transform.rotation = qua;
           Obj.SetActive(true);
       }
       private void Lost()
       {
           Obj.SetActive(false);
       }
     }

:raw-html-m2r:`<br />`


4. :raw-html-m2r:`<font color=#636363>If you want to use your image as a detection target, you could switch the image database in</font>` ``NRTrackableImageTarget.prefab``\ :raw-html-m2r:`<br>`

   .. image:: ../../../images/Emulator/em1-11.jpg


   :raw-html-m2r:`<br />`

  * :raw-html-m2r:`<font color=#636363>In NRSDK, we provided you with three default images for image detection. If you would like to add your own, please find</font>` ``NRCameraRig.prefab``


    * :raw-html-m2r:`<font color=#636363>Find</font>`\ ``NRKernalSessionConfig.asset`` :raw-html-m2r:`<font color=#636363>under the</font>` ``NRSessionBehaviour.cs``


    .. image:: ../../../images/Emulator/em1-12.jpg


    :raw-html-m2r:`<br />`


    * :raw-html-m2r:`<font color=#636363>Find TrackingImageDatabase in the</font>` ``NRKernalSessionConfig.asset``\ :raw-html-m2r:`<font color=#636363>, and drag your own database.asset into it. </font>`


    .. image:: ../../../images/Emulator/em1-13.jpg


    :raw-html-m2r:`<br />`


    * Refer to :ref:`Image Tracking<image_tracking>` for more inforamtion


    .. image:: ../../../images/Emulator/em1-14.jpg

