
Notification popup window
=========================

Ordinary prompt window
------------------------
Ordinary prompt window provides information about the status of the devices, such as slam tracking state, glasses temprature level, device battery state.

|image0|

**Enable Notification**

* Find the NRSessionConfig in your project

  |image1|

* Check the "Enable Notifiction" box

  |image2|

**Notification types**

* Currently provided notifiction windows:

  * Slam tracking lost
  * Glasses Temperature Warm
  * Glasses Temperature Hot
  * Phone Battery 30% Notification 
  * Phone Battery 15% Notification 

* Please refer to `NRNotificationListener` prefab under `NRSDK>Resources` and its `NRNotificationListener.cs` script to customize your notification message.

  |image3|


Error prompt window
-------------------
Error prompt window provides information for sever error. It usually pops up when the devices has been unable to run properly.

|image4|

**Customize error tips**

* Find the `NRErrorTips` prefab under `NRSDK>Resources`, copy it to your project.

* Modify and save it as a new prefab, and drag it to your SessionConfig.
  
  |image5|

  |image6|

* Modify prompt text
  
  .. code-block:: c
    :linenos:

    // Static constants need to be replaced in awake
    void Awake() 
    {
        NativeConstants.GlassesDisconnectErrorTip = "Please connect your Nreal Light Glasses.";
        NativeConstants.UnknowErrorTip = "Unkown error! \nPlease contact Nreal's customer service.";
    }


.. |image0| image:: ../../../images/Notification/n1-0.png
.. |image1| image:: ../../../images/Notification/n1-1.png
.. |image2| image:: ../../../images/Notification/n1-2.png
.. |image3| image:: ../../../images/Notification/n1-3.png
.. |image4| image:: ../../../images/Notification/n1-4.jpg
.. |image5| image:: ../../../images/Notification/n1-5.jpg
.. |image6| image:: ../../../images/Notification/n1-6.jpg