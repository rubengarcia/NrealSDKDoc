.. role:: raw-html-m2r(raw)
   :format: html

.. _image_tracking:


Image Tracking
--------------

Introduction
^^^^^^^^^^^^

:raw-html-m2r:`<font color=#636363>NRSDK's Image Tracking capability helps you build AR apps that can respond to and follow images in the natural environment. These can include books, signs, posters, or logos.</font>`

:raw-html-m2r:`<font color=#636363>A set of images can be uploaded to the database as pre-trained references. These images can then be tracked by NRSDK in the physical world while the transformation of the images are updated during the session.</font>`

:raw-html-m2r:`<br />`

Capabilities
^^^^^^^^^^^^


* 
  :raw-html-m2r:`<font color=#636363>NRSDK can detect and track images that are fixed in place, such as a painting on a wall or a book on the table. It can also track moving images, such as a rotating billboard or a poster held by someone walking around.</font>`

* 
  :raw-html-m2r:`<font color=#636363>After NRSDK begins tracking an image, it provides estimations for position, orientation, and physical size. The image's transformation estimates are updated continuously. Its accuracy increases as NRSDK gathers more data from different angles.</font>`

* 
  :raw-html-m2r:`<font color=#636363>NRSDK can remember up to 10 images in the environment. However, these images cannot be tracked simultaneously in the current version. Therefore, only one image can be tracked in each frame. Additionally, NRSDK will not track multiple instances of the same image.</font>`

:raw-html-m2r:`<br />`

Requirements
^^^^^^^^^^^^

Image Selection Checklist
~~~~~~~~~~~~~~~~~~~~~~~~~


* 
  :raw-html-m2r:`<font color=#636363>Reference images should be in JPEG format, grayscale or RGB color, and have a dpi value of 150. The dimensions of printed reference images should be less than 1m^2.</font>`

* 
  :raw-html-m2r:`<font color=#636363>To enable better tracking, it is important to use images with well-distributed feature points and low degrees of self-similarity. NRSDK can evaluate the tracking quality of the reference images when you add them to the database. Images with low tracking quality cannot be added to the database.</font>`

* 
  :raw-html-m2r:`<font color=#636363>For those who design original reference images, we suggest to export the image using Adobe Illustrator rather than Adobe Photoshop. This is because Photoshop has looser export standards and NRSDK can sometimes fail to read information on pixel density.</font>`

Image Tracking Condition Requirement
""""""""""""""""""""""""""""""""""""

* :raw-html-m2r:`<font color=#636363>Images printed with dull surfaces perform better than glossy ones due to reduced light reflections.</font>`
* :raw-html-m2r:`<font color=#636363>The quickest way to initialize image tracking is to view the image at a slight angle while keeping the image flat and undistorted.</font>`
* :raw-html-m2r:`<font color=#636363>The NRImageTracking Tool gives a score between 0 and 100 for each uploaded reference image. We recommend using images with a score of 65 and above. Using an image with a score lower than 40 will result in poor tracking quality.</font>`

Examples of good images and bad images:
"""""""""""""""""""""""""""""""""""""""


.. image:: ../../../images/imgtrack01.jpg
   :alt: 


:raw-html-m2r:`<br />`

Image Tracking Tutorial
^^^^^^^^^^^^^^^^^^^^^^^

:raw-html-m2r:`<font color=#636363>Learn how to use the Image Tracking feature in your own apps.</font>`

Build and Run the Sample App
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:raw-html-m2r:`<font color=#636363>Create a new project in Unity.</font>`

..

   Need help setting up? Try our `Quickstart for Android Unity </develop/unity/android-quickstart>`_ tutorial.


:raw-html-m2r:`<font color=#636363>Before you build the app, make sure you include the tracking image scene located in</font>`
 ``Assets > NRSDK > Demos > ImageTracking``

:raw-html-m2r:`<font color=#636363>Make sure your Nreal computing unit is connected to your PC before clicking Build and Run. Unity builds your project into an Android APK, which will be installed onto the computing unit. The app can then be launched once you plug Nreal Light glasses into the computing unit.</font>`

:raw-html-m2r:`<font color=#636363>As you move your Nreal Light glasses, the app automatically detects and tracks images from the set of reference images.</font>`

Create the Database File
~~~~~~~~~~~~~~~~~~~~~~~~

:raw-html-m2r:`<font color=#636363>Select one or more pictures you want to use with the mouse, then right-click and select the</font>` ``Create > NRSDK > TrackingImageDatabase`` :raw-html-m2r:`<font color=#636363>menu to create the database file.</font>`


.. image:: ../../../images/imgtrack02.jpg


:raw-html-m2r:`<br />`

:raw-html-m2r:`<font color=#636363>A new image database is created in the same folder as the reference images.</font>`

:raw-html-m2r:`<font color=#636363>The images in the database will be shown in the Inspector window along with image quality scores. We recommend using images with scores higher than 65 to guarantee good tracking performance.</font>`

:raw-html-m2r:`<font color=#636363>The dimensions of the physical image (in meters) for each uploaded reference image should be the same as the dimension shows above.</font>`

:raw-html-m2r:`<font color=#636363>You can click 'x' to delete items.</font>`

**Note:**\ :raw-html-m2r:`<font color=#636363> The size of the images you use must correspond to the size in the database</font>`

Set the Database File
~~~~~~~~~~~~~~~~~~~~~


* 
  :raw-html-m2r:`<font color=#636363>In the Project window, go to</font>` ``Assets > NRSDK`` :raw-html-m2r:`<font color=#636363>and open NRKernalSessionConfig.</font>`

* 
  :raw-html-m2r:`<font color=#636363>In the Inspector window, click the box next to Tracking Image Database and select the database file you created.</font>`

    
  .. image:: ../../../images/imgtrack03.jpg


:raw-html-m2r:`<br />`

Get Tracked Image
~~~~~~~~~~~~~~~~~

:raw-html-m2r:`<font color=#636363>See</font>` ``MarkerDetecter.cs`` :raw-html-m2r:`<font color=#636363>, located in</font>` ``Assets > NRSDK > Demos > TrackingImage > Scripts`` :raw-html-m2r:`<font color=#636363>for an example on how to get the trackables：</font>`

.. code-block:: c#

    // Get a new NRTrackableImage
    NRFrame.GetTrackables<NRTrackableImage>(m_NewTrackableImages, TrackableQueryFilter.New);

    // Get the trackable's position
    NRTrackableImage image = m_NewTrackableImages[i];
    image.GetCenterPose()

    // Get the trackable's size
    image.Size;

    // Get the trackable's state
    image.GetTrackingState();     //Tracking,Paused,Stopped


    // Get all NRTrackableImage
    NRFrame.GetTrackables<NRTrackableImage>(m_AllTrackableImages, TrackableQueryFilter.All);


:raw-html-m2r:`<br />`
