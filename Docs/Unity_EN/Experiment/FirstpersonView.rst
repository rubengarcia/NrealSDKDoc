
First Person View
=================

Operating environment
---------------------

* 【Hardware】 PC/Android mobile phone + Adapted mobile phone or Compute Unit + Nreal Light
* 【Version】 SDK:v1.5.6_beta and above 

Steps
-----


* Import NRSDKForUnityAndroid_Beta_v1.5.12.unitypackage
* Drag "FPSStreamingCast" prefab into your scene

  .. image:: https://codimd.s3.shivering-isles.com/demo/uploads/upload_831224ed5571b8955f3ea317419e82ce.png


* Build and install
* 
  Open the server application 
  **Windows platform :**
  “StreamingReceiver/StreamingReceiver.exe” in pc

  .. image:: https://codimd.s3.shivering-isles.com/demo/uploads/upload_43ba732e2a554f3d312f6878bf8ab3c9.png

  **Android platform :**
  Install the "StreamingReceiver.apk" to a android phone

* 
  Select FirstPersonView Menu.

  .. image:: https://codimd.s3.shivering-isles.com/demo/uploads/upload_b2a10d25c8032e984072b847684b26ba.png

* 
  Start the app build with NRSDK, When the ray is pointed to the top of the field of vision, the menu bar will appear. After you click the "stream" button, it will automatically search the server program in the local LAN, connect it, and then transmit the video stream.

  .. image:: https://codimd.s3.shivering-isles.com/demo/uploads/upload_b58997954316ce8119b28ebb7adf915c.png

Note
----


* Ensure Server and client are in the same local area network.
* Turn off the firewall in server(if windows platform)
