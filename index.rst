.. BookData documentation master file, created by
   sphinx-quickstart on Sun Mar 27 09:02:01 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

NRSDK User Guide
====================================
.. note::

    Here is the NRSDK1.7.0 version of the use of the document, welcome to use.

Catalogue:
^^^^^


.. toctree::
    :maxdepth: 1
    :caption: Discover
 
    Docs/Unity_EN/Discover/IntroducingNRSDK
    Docs/Unity_EN/Discover/Core Features


.. toctree::
    :maxdepth: 1
    :caption: Develop
 
    Docs/Unity_EN/Develop/Quickstart for Android
    Docs/Unity_EN/Develop/Image Tracking
    Docs/Unity_EN/Develop/Controller
    Docs/Unity_EN/Develop/RGB Camera
    Docs/Unity_EN/Develop/Emulator
    Docs/Unity_EN/Develop/VideoCapture
    Docs/Unity_EN/Develop/Customize Phone Controller
    Docs/Unity_EN/Develop/Hand Tracking
    Docs/Unity_EN/Develop/Notification
    Docs/Unity_EN/Develop/NRSDK Coordinate System for Plugins

.. toctree::
    :maxdepth: 1
    :caption: XR Platform
 
    Docs/Unity_EN/XR/Introduction

.. toctree::
    :maxdepth: 1
    :caption: Scripting API
 
    Docs/Unity_EN/ScriptingAPI/Api

.. toctree::
    :maxdepth: 1
    :caption: Experiment
 
    Docs/Unity_EN/Experiment/FirstpersonView
.. Docs/Unity_EN/Experiment/Mapping
.. Docs/Unity_EN/Experiment/Observer View
